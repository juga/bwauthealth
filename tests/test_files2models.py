import logging

import pytest
from bwauthealth.models.bwfile import BwFile

from bwauthealth import files2models

logger = logging.getLogger(__name__)


@pytest.mark.unit_test
@pytest.mark.django_db
def test_document_from_file_bwfile(bwfile_file):
    bwfile = files2models.document_from_file(bwfile_file)
    assert isinstance(bwfile, BwFile)
    logger.debug("bwfile %s", str(bwfile))
    # From here assert on all the `KeyValues`
