from django.contrib import admin

from bwauthealth.models.consensus import Consensus


@admin.register(Consensus)
class ConsensusAdmin(admin.ModelAdmin):
    list_display = ("valid_after", "routerstatuses_count")
    list_filter = ["valid_after"]
