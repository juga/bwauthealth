from django.urls import reverse
from django.utils.html import format_html


def relay_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:bwauthealth_relay_change", args=(obj.pk,)), obj
        )
    )


def relaydesc_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:bwauthealth_relaydesc_change", args=(obj.pk,)), obj
        )
    )


def routerstatus_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:bwauthealth_routerstatus_change", args=(obj.pk,)),
            obj,
        )
    )
