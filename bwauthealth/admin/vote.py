from bwauthealth.models.vote import Vote
from django.contrib import admin


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = (
        "valid_after",
        "bwauth",
        "bwfile",
    )
    list_filter = ["valid_after", "bwauth", "bwfile"]
