from django.contrib import admin

from bwauthealth.models.relaydesc import RelayDesc

from .links import relay_change_link


@admin.register(RelayDesc)
class RelayDescAdmin(admin.ModelAdmin):
    list_display = (
        # "master_key_ed25519",
        "fingerprint",
        "nickname",
        # "created_at",
        "published",
        "relay_link",
        "observed_bandwidth",
        "average_bandwidth",
        "burst_bandwidth",
        "can_exit_443",
        "_can_exit_443_strict",
        "_can_exit_v6_443",
    )
    # ordering = ("-created_at",)
    # list_filter = ["created_at"]
    search_fields = ["nickname", "fingerprint"]

    def relay_link(self, obj):
        if obj.relay:
            return relay_change_link(obj.relay)
        return None

    relay_link.short_description = "Relay"
