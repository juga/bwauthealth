from django.contrib import admin

from bwauthealth.models.relay import Relay


@admin.register(Relay)
class RelayAdmin(admin.ModelAdmin):
    list_display = (
        "fingerprint",
        "relaybws_count",
        "consensuses_count",
        "routerstatuses_count",
        "relaydescs_count",
        "is_exit",
    )
    search_fields = ["fingerprint"]
    readonly_fields = ["relaybws", "routerstatuses", "relaydescs"]
