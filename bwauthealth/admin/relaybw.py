from bwauthealth.models.relaybw import RelayBw
from django.contrib import admin

from .links import (
    relay_change_link,
    relaydesc_change_link,
    routerstatus_change_link,
)


class IsRouterstatusUpdatedFilter(admin.SimpleListFilter):
    title = "Is routerstatus updated"
    parameter_name = "is_routerstatus_updated"

    def lookups(self, request, model_admin):
        return (("Yes", "Yes"), ("No", "No"))

    def queryset(self, request, queryset):
        fps = [
            rb.fingerprint
            for rb in queryset.all()
            if rb.is_routerstatus_updated()
        ]
        if self.value() == "Yes":
            return queryset.filter(fingerprint__in=fps)
        elif self.value() == "No":
            return queryset.exclude(fingerprint__in=fps)
        return queryset.all()


@admin.register(RelayBw)
class RelayBwAdmin(admin.ModelAdmin):
    list_display = (
        "fingerprint",
        "nickname",
        "measured_at",
        # "updated_at",
        # "master_key_ed25519",
        "bwfile",
        # "bwauth",
        "bw",
        "_ratio",
        "_min_descs_bw",
        # "error_stream",
        # "error_circ",
        # "error_misc",
        # "error_second_relay",
        # "error_destination",
        # "bw_mean",
        # "desc_bw_avg",
        "desc_bw_obs_last",
        "desc_bw_obs_mean",
        # "desc_bw_bur",
        "consensus_bandwidth",
        "consensus_bandwidth_is_unmeasured",
        # "relay_in_recent_recent_consensus_count",
        "relay_recent_priority_list_count",
        "relay_recent_measurement_attempt_count",
        # "relay_recent_measurement_failure_count",
        "relay_recent_measurements_excluded_error_count",
        # "relay_recent_measurements_excluded_near_count",
        # "relay_recent_measurements_excluded_old_count",
        # "relay_recent_measurements_excluded_few_count",
        "success",
        "vote",
        # "unmeasured",
        # "under_min_report",
        # "is_relaydesc_updated",
        # "last_relaydesc_change_link",
        # "is_routerstatus_updated",
        # "last_routerstatus_change_link",
        "is_in_consensus",
        "is_exit",
    )
    list_display_links = ("fingerprint",)
    list_filter = [
        # "created_at",
        "vote",
        # "relaydesc_updated",
        # "bwfile",
        "bwfile__bwauth",
        # "bwfile__consensus",
        # IsRouterstatusUpdatedFilter,
    ]
    search_fields = ["nickname", "fingerprint"]

    def relay_link(self, obj):
        if obj.relay:
            return relay_change_link(obj.relay)
        return None

    relay_link.short_description = "Relay"

    def last_relaydesc_change_link(self, obj):
        d = obj.last_relaydesc()
        if d:
            return relaydesc_change_link(d)
        return None

    relay_link.short_description = "Last descriptor"

    def last_routerstatus_change_link(self, obj):
        d = obj.last_routerstatus()
        if d:
            return routerstatus_change_link(d)
        return None

    last_routerstatus_change_link.short_description = "Last router status"
