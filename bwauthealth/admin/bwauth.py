from django.contrib import admin

from bwauthealth.models.bwauth import BwAuth


@admin.register(BwAuth)
class AuthorityAdmin(admin.ModelAdmin):
    list_display = (
        "nickname",
        # "fingerprint",
        # "ipv4",
        # "contact",
        # "recent_consensus_count",
    )
