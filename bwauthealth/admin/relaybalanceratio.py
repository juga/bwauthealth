from bwauthealth.models.relaybalanceratio import RelayBalanceRatio
from django.contrib import admin


@admin.register(RelayBalanceRatio)
class RelayBalanceRatioAdmin(admin.ModelAdmin):
    list_display = (
        "fingerprint",
        "nickname",
        "published",
        "valid_after",
        "observed_bandwidth",
        "average_bandwidth",
        "burst_bandwidth",
        "measured",
        "bwauth_nickname",
        "_min_descs_bw",
        "_relay_type",
        "_ratio",
    )
    search_fields = ["nickname", "fingerprint"]
    list_filter = ["bwauth_nickname", "valid_after"]
