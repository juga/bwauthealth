from bwauthealth.models.bwfile import BwFile
from django.contrib import admin


@admin.register(BwFile)
class BwFileAdmin(admin.ModelAdmin):
    # pass
    list_display = (
        "file_created",
        "consensus",
        #         # "file_hash",
        "bwauth",
        "scanner_country",
        #         # 'destinations_countries',
        #         # "software",
        "software_version",
        "bw_sum",
        "consensus_routerstatuses_count",
        # "relaybw_set_count",
        "relaybw_set_vote_count",
    )
    list_filter = [
        "bwauth",
        "consensus",
        #         "relaybw__relaydesc_updated",
        #         "relaybw__routerstatus_updated",
    ]
