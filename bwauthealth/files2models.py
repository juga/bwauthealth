import logging
import os
from datetime import datetime

from stem import descriptor

from .models import BwFile, Consensus, RelayBalanceRatio, RelayDesc, Vote

logger = logging.getLogger(__name__)


def documents_from_dir(dir_path, start=None, end=None):
    documents = []
    logger.info("Searching for document in period %s-%s", start, end)
    for file in os.listdir(dir_path):
        file_path = os.path.join(dir_path, file)
        if (
            os.path.isfile(file_path)
            and not file_path.endswith(".tar")
            and not file_path.endswith(".xz")
        ):
            date = file[:19]
            file_date = datetime.strptime(date, "%Y-%m-%d-%H-%M-%S")
            if start:
                if start > file_date:
                    continue
            if end:
                if file_date > end:
                    continue
            logger.info("File matching date period %s", file)
            documents.append(file_path)
    return documents


def document_from_file(
    filepath,
    into_relay_balance_ratio=True,
    bwauth_nickname=None,
    relay_fp=None,
):
    """Convert a file document into its object.

    :param bool into_relay_balance_ratio: Whether to create RelayBalanceRatio
    objects from the document

    """
    logger.debug("%s", filepath)
    try:
        document = list(descriptor.parse_file(filepath))
    except TypeError:
        # stemize_document(filepath)
        try:
            document = list(
                descriptor.parse_file(filepath, "bandwidth-file 1.0")
            )
        except Exception as e:
            logger.exception(e)
            return
    except Exception as e:
        logger.exception(e)
        return
    if not document:
        return
    if not isinstance(document, list):
        return
    obj = None
    if isinstance(document[0], descriptor.bandwidth_file.BandwidthFile):
        logger.info("It's a bandwidth file.")
        if into_relay_balance_ratio:
            obj = RelayBalanceRatio.objects.from_bandwidth_file(
                document[0], bwauth_nickname, relay_fp
            )
            return obj
        obj = BwFile.objects.from_bandwidth_file(document[0])
        logger.info("Created BwFile %s", obj)
        return obj
    if isinstance(document[0], descriptor.server_descriptor.RelayDescriptor):
        # logger.info("It's relay descriptor(s) %s.", document)
        if into_relay_balance_ratio:
            RelayBalanceRatio.objects.from_relay_descs(document)
            return obj
        obj = RelayDesc.objects.from_relay_descs(document)
        # logger.info("Created RelayDescriptor %s", obj)
        return obj
    if document[0].document.is_consensus:
        logger.info("It's a consensus.Number relays %s", len(document))
        obj = Consensus.objects.from_router_statuses(document)
        logger.info("Created consensus %s", obj)
    if document[0].document.is_vote:
        logger.info("It's a vote.Number relays %s", len(document))
        if into_relay_balance_ratio:
            obj = RelayBalanceRatio.objects.from_router_statuses(document)
            return obj
        obj = Vote.objects.from_router_statuses(document)
        logger.info("Created vote %s", obj)
        return obj


def documents_from_filepath(filepath, bwauth_nickname=None, relay_fp=None):
    logger.info("Parsing file(s) from %s", filepath)
    if os.path.isfile(filepath):
        return document_from_file(filepath, True, bwauth_nickname, relay_fp)
    if os.path.isdir(filepath):
        for dp, _, filenames in os.walk(filepath):
            for f in filenames:
                document_from_file(
                    os.path.join(dp, f), True, bwauth_nickname, relay_fp
                )
