import datetime
import logging

from django.conf import settings
from django.core.management.base import BaseCommand

from bwauthealth import graphs, util

logger = logging.getLogger(__name__)
now = datetime.datetime.utcnow().strftime("%Y%m%dT%H%M")


class Command(BaseCommand):
    help = "Graph CDF relay bandwidth ratio."

    def add_arguments(self, parser):
        parser.add_argument(
            "-o",
            "--graph-path",
            default=settings.CDF_GRAPH_PATH,
            help="Path to the svg output file.",
        )
        parser.add_argument(
            "-r", "--dtranges", type=util.dtrange_split, nargs="*"
        )
        parser.add_argument("-n", "--bwauth-nickname", default="")

    def handle(self, *args, **options):
        logger.debug("Arguments %s.", options.keys())
        logger.info("CdfGraph.")
        dtranges = options.get("dtranges", None)
        if dtranges and isinstance(dtranges[0], datetime.datetime):
            dtranges = [dtranges]
        logger.info("dtranges %s", dtranges)
        bwauth_nickname = options.get("bwauth_nickname", "")
        logger.info("bwauth_nickname %s", bwauth_nickname)
        figure = graphs.bwauths_cdf_relay_balance_ratio(
            bwauth_nickname, dtranges
        )
        if not figure:
            logger.info("Could not create graph.")
            return
        graph_path = options["graph_path"].format(
            bwauth_nickname, util.dtranges2isostr(dtranges)
        )
        figure.write_image(graph_path, format="svg")
        logger.info("Written %s", graph_path)
