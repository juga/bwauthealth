import logging
import sys

from bwauthealth.files2models import documents_from_filepath
from django.core.management.base import BaseCommand

logger = logging.getLogger()


class Command(BaseCommand):
    help = "Import cached descriptor documents."

    def add_arguments(self, parser):
        parser.add_argument("-f", "--filepath")
        parser.add_argument("-n", "--bwauth-nickname", default="")
        parser.add_argument("-p", "--relay-fingerprint", default="")

    def handle(self, *args, **options):
        logger.debug("Arguments %s.", options.keys())

        bwauth_nickname = options.get("bwauth_nickname", "")
        relay_fp = options.get("relay_fingerprint", "")

        if options["filepath"]:
            documents_from_filepath(
                options["filepath"], bwauth_nickname, relay_fp
            )
            sys.exit(0)
