# Fetch is done separately from import to cache the files and don't get
# stem exception about the hash of the file.
import logging

from django.conf import settings
from django.core.management.base import BaseCommand

from bwauthealth.fetch import (
    fetch_bwfiles,
    fetch_consensuses,
    fetch_relaydescs,
)
from bwauthealth.util import valid_datetime

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Fetch relaydesc_ documents and cache them."

    def add_arguments(self, parser):
        parser.add_argument(
            "-p",
            "--cache-path",
            default=settings.CACHE_PATH,
            help="Path to the cache dir.",
        )
        parser.add_argument("-b", "--bwfiles", action="store_true")
        parser.add_argument("-c", "--consensuses", action="store_true")
        parser.add_argument("-d", "--descriptors", action="store_true")
        parser.add_argument("-e", "--end", type=valid_datetime)
        parser.add_argument("-s", "--start", type=valid_datetime)

    def handle(self, *args, **options):
        logger.debug("Arguments %s.", options.keys())
        if (
            not options["bwfiles"]
            and not options["consensuses"]
            and not options["descriptors"]
        ):
            options["bwfiles"] = options["consensuses"] = options[
                "descriptors"
            ] = True

        start = options["start"]
        end = options["end"]

        if options["consensuses"]:
            fetch_consensuses(start, end)
        if options["descriptors"]:
            fetch_relaydescs(start, end)
        if options["bwfiles"]:
            fetch_bwfiles(start, end)
