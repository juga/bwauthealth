import datetime
import logging

from django.core.management.base import BaseCommand

from bwauthealth import stats, util

logger = logging.getLogger(__name__)
now = datetime.datetime.utcnow().strftime("%Y%m%dT%H%M")


class Command(BaseCommand):
    help = "Relays' stream bandwidth statistics."

    def add_arguments(self, parser):
        parser.add_argument(
            "-r",
            "--dtrange",
            type=util.dtrange_split,
            help="eg: 2022-06-13T13:00:00,2022-06-13T14:00:00",
        )

    def handle(self, *args, **options):
        logger.debug("Arguments %s.", options.keys())
        dtrange = options.get("dtrange", None)
        logger.info("dtrange: %s", dtrange)
        stats.bwauths_stats(dtrange)
