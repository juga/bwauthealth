import csv
import datetime
import logging
import os.path
import sys

from django.conf import settings
from django.core.management.base import BaseCommand

from bwauthealth.report import (
    bw_comparative_over_under_weighted,
    missing_relays,
    missing_relays_header,
    relays_in_bwfile_not_in_bwfile_other,
    relays_in_bwfile_not_in_bwfile_other_header,
    report,
    report_header,
    sbws_missing_relays,
    sbws_missing_relays_header,
)
from bwauthealth.util import valid_datetime

logger = logging.getLogger(__name__)
now = datetime.datetime.utcnow().strftime("%Y%m%dT%H%M")


class Command(BaseCommand):
    help = "Report relays in consensus missing in the bandwidth file."

    def add_arguments(self, parser):
        parser.add_argument(
            "-f",
            "--report-path",
            default=os.path.join(settings.MAIN_REPORT_PATH.format(now)),
            help="Path to the csv output file.",
        )
        parser.add_argument(
            "-m",
            "--missing-relays-path",
            default=os.path.join(settings.MISSING_RELAYS_PATH.format(now)),
            help="Path to the csv output file.",
        )
        parser.add_argument(
            "-b",
            "--bw-comparative",
            default=os.path.join(settings.BW_COMPARATIVE_PATH.format(now)),
            help="Path to the csv output file.",
        )
        parser.add_argument(
            "-o",
            "--order-magnitude",
            default=10,
            help="Magnitude difference between sbws and torflow bandwidth.",
        )
        parser.add_argument("-e", "--end", type=valid_datetime)
        parser.add_argument("-s", "--start", type=valid_datetime)
        parser.add_argument("--one", type=valid_datetime)
        parser.add_argument("--other", type=valid_datetime)

    def handle(self, *args, **options):
        logger.debug("Arguments %s.", options.keys())
        logger.info("Report.")

        rows = []
        if options["one"] and options["other"]:
            rows = relays_in_bwfile_not_in_bwfile_other(
                options["one"], options["other"]
            )
            if len(rows) < 1:
                logger.info(
                    "There was no data. Try fetch and import commands."
                )
                sys.exit()
            rows.insert(0, relays_in_bwfile_not_in_bwfile_other_header())
            out = settings.RELAYS_BWFILE_NOT_IN_OTHER.format(now)
            with open(out, "w") as fd:
                csv_writer = csv.writer(fd)
                for row in rows:
                    csv_writer.writerow(row)
            logger.info("Written %s", out)
            sys.exit()

        rows = report(options["start"], options["end"])
        if len(rows) < 1:
            logger.info("There was no data. Try fetch and import commands.")
        rows.insert(0, report_header())
        with open(options["report_path"], "w") as fd:
            csv_writer = csv.writer(fd)
            for row in rows:
                csv_writer.writerow(row)
        logger.info("Written %s", options["report_path"])

        rows = missing_relays(options["start"], options["end"])
        # logger.info("Missing relays from consensus.")
        # if len(rows) < 1:
        #     logger.info("There was no data. Try fetch and import commands.")
        #     sys.exit()
        # rows.insert(0, missing_relays_header())
        # with open(options["missing_relays_path"], "w") as fd:
        #     csv_writer = csv.writer(fd)
        #     for row in rows:
        #         csv_writer.writerow(row)
        # logger.info("Written %s", options["missing_relays_path"])

        logger.info("sbws missing relays.")
        rows = sbws_missing_relays(options["start"], options["end"])
        if len(rows) < 1:
            logger.info("There was no data. Try fetch and import commands.")
        rows.insert(0, sbws_missing_relays_header())
        with open(options["missing_relays_path"], "w") as fd:
            csv_writer = csv.writer(fd)
            for row in rows:
                csv_writer.writerow(row)
        logger.info("Written %s", options["missing_relays_path"])

        # rows = bw_comparative2(
        #     options["start"], options["end"], int(options["order_magnitude"])
        # )
        # with open(options["bw_comparative"], "w") as fd:
        #     csv_writer = csv.writer(fd)
        #     for row in rows:
        #         csv_writer.writerow(row)
        # logger.info("Written %s", options["bw_comparative"])

        rows = bw_comparative_over_under_weighted(
            options["start"], options["end"], int(options["order_magnitude"])
        )
        with open(options["bw_comparative"], "w") as fd:
            csv_writer = csv.writer(fd)
            for row in rows:
                csv_writer.writerow(row)
        logger.info("Written %s", options["bw_comparative"])
