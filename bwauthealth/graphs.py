import logging

import pandas
import plotly.express as px

from . import util
from .models import BwAuth, BwFile, RelayBalanceRatio

logger = logging.getLogger(__name__)


def bwauth_relay_balance_ratio(
    bwauth_nickname, dtrange, max_ratio=3, min_ratio=None, relay_fp=None
):
    logger.info("")
    # ratios = bwauth.relay_balance_ratios(start, end)
    relay_balance_ratios = RelayBalanceRatio.objects.filter(
        bwauth_nickname=bwauth_nickname
    )
    if dtrange:
        logger.debug("dtrange %s", dtrange)
        relay_balance_ratios = relay_balance_ratios.filter(
            valid_after__range=dtrange
        )
    if relay_fp:
        logger.debug("relay fingerprint %s", relay_fp)
        relay_balance_ratios = relay_balance_ratios.filter(
            fingerprint=relay_fp
        )
    logger.debug("%s relay balance ratios", relay_balance_ratios.count())
    if (relay_balance_ratios.filter(_ratio__isnull=False).count()) > 0:
        logger.debug(
            "%s relay balance ratios with calculated ratios",
            relay_balance_ratios.filter(_ratio__isnull=False).count(),
        )
    else:
        logger.debug("no relay balance ratios, calculating them")
        # In case there're RelayBalanceRatios without _ratio
        if relay_balance_ratios.count() > 0:
            logger.debug("but relay balance ratios objects were created")
            for rbr in relay_balance_ratios:
                rbr.set_relay_balance_ratio()
        # In case there aren't RelayBalanceRatios
        else:
            # In case there're bwfiles
            bwfiles = BwFile.objects.filter(
                bwauth__nickname=bwauth_nickname, file_created__range=dtrange
            )
            logger.debug("Calculating ratios from relaybws.")
            if bwfiles:
                for bwfile in bwfiles:
                    RelayBalanceRatio.objects.from_relaybws(
                        bwfile.relaybw_set.all()
                    )
                    rbrs = RelayBalanceRatio.objects.filter(
                        bwauth_nickname=bwauth_nickname,
                        valid_after=bwfile.file_created.replace(
                            minute=0, second=0
                        ),
                    )
                    [rbr.set_relay_balance_ratio for rbr in rbrs]
            # # In case there're votes and descriptors
            # votes =  Vote.objects.filter(bwauth__nickname=bwauth_nickname,
            # valid_after__range=dtrange)
            # relaydescs = RelayDesc.objects.filter(published__range=dtrange)
            # if votes and relaydescs:
            #     for vote in votes:
            #        rbr = RelayBalanceRatio.from_router_statuses()
    relay_balance_ratios = RelayBalanceRatio.objects.filter(
        bwauth_nickname=bwauth_nickname,
        valid_after__range=dtrange,
        _ratio__isnull=False,
    )
    ratios = relay_balance_ratios.values_list("_ratio", flat=True)
    if max_ratio:
        ratios = ratios.filter(_ratio__lt=max_ratio)
    if min_ratio:
        ratios = ratios.filter(_ratio_gte=min_ratio)

    logger.info("ratios %s", len(ratios))
    return ratios


def bwauths_relay_balance_ratio(
    bwauth_nickname, dtranges, max_ratio=3, min_ratio=None, relay_fp=None
):
    """
    If more than a datetime dtrange is past, graph every bwauth within those
    dtrange.
    If only one datetime dtrange is past, graph all bwauths together in that
    dtrange.

    """
    logger.info(
        "Calculating CDF relay-balance-ratio for %s bwauth(s) during the"
        " period %s.",
        bwauth_nickname or "all",
        util.dtranges2isostr(dtranges),
    )
    ratios_dict = {}

    if not bwauth_nickname:
        bwauths = BwAuth.objects.all()
        # XXXX: Only one range when taking all bwauths
        dtrange = dtranges[0]
        title = util.dtrange2isostr(dtrange)
        for bwauth in bwauths:
            logger.info("bwauth %s", bwauth.nickname)
            ratios_dict[bwauth.nickname] = bwauth_relay_balance_ratio(
                bwauth.nickname, dtrange, max_ratio, min_ratio, relay_fp
            )
    else:
        bwauth = BwAuth.objects.get(nickname=bwauth_nickname)
        title = bwauth_nickname
        for dtrange in dtranges:
            logger.info("dtrange %s", dtrange)
            ratios_dict[
                util.dtrange2isostr(dtrange)
            ] = bwauth_relay_balance_ratio(
                bwauth.nickname, dtrange, max_ratio, min_ratio, relay_fp
            )
    logger.info("title: %s, len ratios dict: %s", title, len(ratios_dict))
    return title, ratios_dict


def bwauths_cdf_relay_balance_ratio(
    bwauth_nickname, dtranges, max_ratio=3, min_ratio=None
):
    # Create dataframe with arrays of different size (adds NaN to missing ratio
    # values)
    title, ratios_dict = bwauths_relay_balance_ratio(
        bwauth_nickname, dtranges, max_ratio, min_ratio
    )
    if not ratios_dict:
        return None
    if len(dtranges) < 2:
        df = pandas.DataFrame(
            dict([(k, pandas.Series(v)) for k, v in ratios_dict.items()]),
            columns=list(ratios_dict.keys()),
        )
        figure = px.ecdf(
            df,
            x=list(ratios_dict.keys()),
            ecdfnorm="percent",
            # color=bwauth_nick,
            title=title,
            width=400,
            height=450,
            markers=True,
        )
        # figure.update_xaxes(dtick=0.5, title_text="ratio")
        figure.update_xaxes(title_text="ratio")
        figure.update_yaxes(dtick=25)
        figure.update_layout(
            legend_title="CDF relay balance ratio",
        )
    else:
        df = pandas.DataFrame(
            dict([(k, pandas.Series(v)) for k, v in ratios_dict.items()]),
            columns=[util.dtrange2isostr(dtrange) for dtrange in dtranges],
        )
        figure = px.ecdf(
            df,
            x=[util.dtrange2isostr(dtrange) for dtrange in dtranges],
            ecdfnorm="percent",
            title=title,
            width=400,
            height=450,
            markers=True,
        )
        # figure.update_xaxes(dtick=0.5, title_text="ratio")
        figure.update_xaxes(title_text="ratio")
        figure.update_yaxes(dtick=25)
        figure.update_layout(
            legend_title="CDF relay balance ratio",
        )
    # figure.show()
    return figure
