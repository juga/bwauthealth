from django.apps import AppConfig


class DjunmeasuredConfig(AppConfig):
    name = "bwauthealth"
    default_auto_field = "django.db.models.BigAutoField"
