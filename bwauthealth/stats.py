import logging

import pandas
from django.db.models import Avg, Max, Min
from django.db.models.aggregates import StdDev

from bwauthealth.models import BwAuth, RelayBalanceRatio

from . import graphs

logger = logging.getLogger(__name__)


def bwauths_lower_quartile(bwauth_nickname, dtrange):
    """Return the lower quartile ratios for a bwauth in datetime range.

    Example output::

    {'(2022-06-13T13:00:00,2022-06-13T14:00:00)':
        [0.5697440113450276, 0.6510416666666666, ...]
    }

    """
    title, ratios_dict = graphs.bwauths_relay_balance_ratio(
        bwauth_nickname, [dtrange]
    )
    df = pandas.DataFrame(
        dict([(k, pandas.Series(v)) for k, v in ratios_dict.items()]),
        columns=list(ratios_dict.keys()),
    )
    quartile_dict = dict(
        [(bwauth, df[bwauth].quantile(0.25)) for bwauth in df.keys()]
    )
    quartile_ratios_dict = dict(
        [
            (k, [i for i in v if i < quartile_dict[k]])
            for (k, v) in ratios_dict.items()
        ]
    )
    return quartile_ratios_dict


def bwauths_lower_quartile_relays(dtrange):
    """Log the number and type of relays with ratios in the lower quartile."""
    rbrs = RelayBalanceRatio.objects.filter(valid_after__range=dtrange)
    rbrs_quartile = rbrs.filter(_ratio__lt=0.72)
    rbrs_quartile_tuples_list = rbrs_quartile.values_list(
        "fingerprint", "_relay_type"
    )
    rbrs_quartile_dict = dict(rbrs_quartile_tuples_list)
    rbrs_quartile_dict_count = len(rbrs_quartile_dict)
    logger.info(
        "Number of relays with ration under first quartile: %s",
        rbrs_quartile_dict_count,
    )
    rbrs_quartile_dict_guards_count = len(
        list(filter(lambda kv: kv[1] == "G", rbrs_quartile_dict.items()))
    )
    logger.info(
        "Number of relays with ratio under first quartile that are guards: %s",
        rbrs_quartile_dict_guards_count,
    )


def bwauths_stats(dtrange):
    """
    Print stream bandwidth statistics for every bwauth in a datetime period.

    """
    all_bwauths_dict = {}
    logger.info("dtrange %s", dtrange)
    for bwauth in BwAuth.objects.all():
        # all_bwauths_dict[bwauth] = bwauth_dict
        logger.debug("bwauth %s", bwauth)
        all_bwauths_dict[bwauth] = {}
        qs = bwauth.bwfile_set.filter(
            file_created__range=dtrange, relaybw__vote=1
        )
        all_bwauths_dict[bwauth]["max"] = qs.aggregate(
            Max("relaybw__bw_median")
        )
        all_bwauths_dict[bwauth]["min"] = qs.aggregate(
            Min("relaybw__bw_median")
        )
        all_bwauths_dict[bwauth]["avg"] = qs.aggregate(
            Avg("relaybw__bw_median")
        )
        all_bwauths_dict[bwauth]["stdev"] = qs.aggregate(
            StdDev("relaybw__bw_median")
        )
    for bwauth, values in all_bwauths_dict.items():
        print("{}:".format(bwauth.nickname))
        for key, value in values.items():
            print("  {}: {}".format(key, list(value.values())[0]))
