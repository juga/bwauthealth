import logging

from statistics import median

from django.conf import settings

from . import util
from .models.bwfile import BwFile
from .models.consensus import Consensus
from .models.relay import Relay
from .models.relaybw import RelayBw

logger = logging.getLogger(__name__)


def report_header():
    return [
        "consensus",
        "bwfile",
        "bwauth",
        "bw_sum",
        "consensus relays",
        "bwfile relays",
        "bwfile vote relays",
        # "relays not in consensus",
        # "relays to vote not in consensus",
        "descriptors updated",
        "router statuses updated",
    ]


def report(start=None, end=None):
    if not start:
        start = util.previous_consensus(settings.NOW)
    if not end:
        end = util.end_dt(start)
    logger.info("Time period from %s to %s", start, end)
    rows = []
    bwfiles = BwFile.objects.filter(
        file_created__gte=start, file_created__lt=end
    )
    for bwfile in bwfiles:
        logger.debug("Bw desc %s", bwfile)
        try:
            consensus = Consensus.objects.get(bwfile=bwfile)
        except Consensus.DoesNotExist as e:
            logger.warning(e)
            consensus = None
        row = [
            consensus,
            bwfile,
            bwfile.bwauth,
            bwfile.bw_sum(),
            bwfile.consensus_routerstatuses_count(),
            bwfile.relaybw_set_count(),
            bwfile.relaybw_set_vote_count(),
            # bwfile.consensus_relays_not_in_bwdesc_count(),
            # bwfile.consensus_relays_vote_not_in_bwdesc_count(),
            bwfile.relaybws_relaydesc_updated_count(),
            bwfile.relaybws_routerstatuses_updated_count(),
        ]
        rows.append(row)
    return rows


def missing_relays_header():
    return ["consensus", "bwfile", "fp"]


def missing_relays(start=None, end=None):
    if not start:
        start = util.previous_consensus(settings.NOW)
    if not end:
        end = util.end_dt(start)
    logger.info("Time period from %s to %s", start, end)
    rows = []
    bwfiles = BwFile.objects.filter(
        file_created__gte=start, file_created__lt=end
    )
    for bwfile in bwfiles:
        logger.debug("Bw desc %s", bwfile)
        consensus = Consensus.objects.get(bwfile=bwfile)
        for fp in bwfile.consensus_relays_not_here():
            row = [consensus, bwfile, fp]
            rows.append(row)
    return rows


def sbws_missing_relays_header():
    return ["consensus", "fp", "nickname"]


def sbws_missing_relays(start=None, end=None):
    if not start:
        start = util.previous_consensus(settings.NOW)
    if not end:
        end = util.end_dt(start)
    logger.info("Time period from %s to %s", start, end)
    bwfiles = BwFile.objects.filter(
        file_created__gte=start, file_created__lt=end
    )

    torflow_bwfiles = bwfiles.filter(scanner_country__isnull=True)
    logger.debug("Torflow bwfiles: %s", torflow_bwfiles)
    sbws_bwfiles = bwfiles.filter(scanner_country__isnull=False)
    logger.debug("sbws bwfiles: %s", sbws_bwfiles)
    torflow_relays = set()
    for bwfile in torflow_bwfiles:
        torflow_relays = torflow_relays.union(
            bwfile.relaybw_set.values_list("fingerprint", flat=True)
        )
    sbws_relays = set()
    for bwfile in sbws_bwfiles:
        sbws_relays = sbws_relays.union(
            bwfile.relaybw_set.values_list("fingerprint", flat=True)
        )
    missing_relays = torflow_relays.difference(sbws_relays)
    try:
        consensus = Consensus.objects.get(
            valid_after__gte=start, valid_after__lt=end
        )
    except Consensus.DoesNotExist as e:
        logger.warning(e)
        consensus = None
    rows = []
    # Here is not possible to say a bwfile cause they've been aggregated
    # obtain the nickname from the consensus router status, since different
    # bwfiles could have different nicknames for the same fingerprint
    for fp in missing_relays:
        try:
            rs = consensus.routerstatus_set.get(fingerprint=fp)
        except:  # DoesNotExist
            rs = None
        nickname = rs.nickname if rs else None
        row = [consensus, fp, nickname]
        rows.append(row)
    return rows


def bw_comparative(start=None, end=None):
    if not start:
        start = util.previous_consensus(settings.NOW)
    if not end:
        end = util.end_dt(start)
    logger.info("Time period from %s to %s", start, end)
    rows = []
    consensuses = Consensus.objects.filter(
        valid_after__gte=start, valid_after__lt=end
    )
    for consensus in consensuses:
        logger.debug("Consensus %s", consensus)
        bwfiles = consensus.bwfile_set.all()
        header = ["fingerprint"] + [bwfile for bwfile in bwfiles]
        rows = [header]
        for rs in consensus.routerstatuses():
            relay = rs.relay
            row = [relay.fingerprint]
            for bwfile in bwfiles:
                try:
                    relaybw = relay.relaybw_set.get(bwfile=bwfile)
                    row.append(relaybw.bw)
                except:
                    row.append("")
            rows.append(row)
    return rows


def bw_comparative2(start=None, end=None, order_magnitude=10):
    """
    Check whether sbws bwauths deviate from torflow one more than an order of
    magnitude and whether it is due to missing descriptor's bandwidth.

    """
    if not start:
        start = util.previous_consensus(settings.NOW)
    if not end:
        end = util.end_dt(start)
    logger.info("Time period from %s to %s", start, end)
    rows = ["fingerprint"]
    consensuses = Consensus.objects.filter(
        valid_after__gte=start, valid_after__lt=end
    )
    rows = []
    for consensus in consensuses:
        header = []
        logger.debug("Consensus %s", consensus)
        bwfiles_torflow = (
            consensus.bwfiles()
            .filter(scanner_country__isnull=True)
            .order_by("-file_created")
        )
        [header.append(bf.bwauth or "torflow") for bf in bwfiles_torflow]
        bwfiles_sbws = (
            consensus.bwfiles()
            .filter(scanner_country__isnull=False)
            .order_by("-file_created")
        )
        [header.append(bf.bwauth or "sbws") for bf in bwfiles_torflow]

        for routerstatus in consensus.routerstatuses():
            row = [routerstatus.fingerprint]  # fingerprint
            relay = routerstatus.relay
            bws = []
            for bwfile in bwfiles_torflow:
                try:
                    relaybw = relay.relaybw_set.get(bwfile=bwfile)
                    row.append(relaybw.bw)
                    bws.append(relaybw.bw)
                except:
                    row.append(0)
            m = median(bws) if bws else 1
            for bwfile in bwfiles_sbws:
                try:
                    relaybw = relay.relaybw_set.get(bwfile=bwfile, vote=True)
                    row.append(relaybw.bw)
                    # overweighted
                    if relaybw.bw / m > order_magnitude and m > 1:
                        row.append(1)
                        if relaybw.desc_bw_obs_last:
                            logger.warning(
                                "Overweighted with observed bandwidth!!! %s",
                                relay.fingerprint,
                            )
                    else:
                        row.append(0)
                    # underweighted
                    if m / relaybw.bw > order_magnitude:
                        row.append(1)
                    else:
                        row.append(0)
                    row.append(relaybw.consensus_bandwidth or 0)
                    row.append(relaybw.desc_bw_obs_last or 0)
                    row.append(relaybw.desc_bw_avg or 0)
                    row.append(relaybw.desc_bw_bur or 0)
                except:
                    row.extend([0, 0, 0, 0, 0, 0, 0])
            rows.append(row)
    sum_nobw_maatuska = len([r[5] for r in rows if r[5] == 0])
    sum_high_maatuska = sum([r[6] for r in rows])
    sum_low_maatuska = sum([r[7] for r in rows])
    sum_nobw_sbws = len([r[12] for r in rows if r[12] == 0])
    sum_high_sbws = sum([r[13] for r in rows])
    sum_low_sbws = sum([r[14] for r in rows])
    row = [
        0,
        0,
        0,
        0,
        0,
        sum_nobw_maatuska,
        sum_high_maatuska,
        sum_low_maatuska,
        0,
        0,
        0,
        0,
        sum_nobw_sbws,
        sum_high_sbws,
        sum_low_sbws,
    ]
    rows.append(row)
    return rows


def bw_comparative_over_under_weighted(
    start=None, end=None, order_magnitude=10
):
    """
    For each relay in the consensus the bwauths used, report its bandwidth and
    whether it is over/underweighted.
    Report also the sum of all the bandwidths and the number of relays
    over/underweighted for each bwauth
    """
    if not start:
        start = util.previous_consensus(settings.NOW)
    if not end:
        end = util.end_dt(start)
    logger.info("Time period from %s to %s", start, end)
    consensuses = Consensus.objects.filter(
        valid_after__gte=start, valid_after__lt=end
    )
    rows = []
    for consensus in consensuses:
        logger.debug("Consensus %s", consensus)
        header = ["fingerprint", "nickname", "median"]
        bwfiles = consensus.bwfiles()
        for bf in bwfiles:
            header.extend([bf.bwauth or "", "overweighted", "underweighted"])
        logger.debug("Header: %s", header)
        # not the relays that the previous consensus have, but the ones that
        # were reported by the bwauths using that consensus
        fps = set(
            Relay.objects.filter(
                relaybw__bwfile__consensus=consensus
            ).values_list("fingerprint", flat=True)
        )
        relays = Relay.objects.filter(fingerprint__in=fps)
        for relay in relays:
            m = median(relay.relaybw_set.values_list("bw", flat=True) or [0])
            nickname = ""
            try:
                nickname = relay.relaybw_set.filter(nickname__isnull=False)[
                    0
                ].nickname
            except:
                pass
            row = [relay.fingerprint, nickname, m]
            for bwfile in bwfiles:
                bw = 0
                try:
                    bw = relay.relaybw_set.get(bwfile=bwfile).bw
                except:
                    pass
                # if overweighted(bw, m) or underweighted(bw, m):
                row.append(bw)
                row.append(overweighted(bw, m))
                row.append(underweighted(bw, m))
            rows.append(row)
    # order by median
    rows.sort(key=lambda x: x[2])
    sum_row = ["", ""]
    # last row: sum of all values
    for i in range(2, len(row)):
        sum_row.append(sum([r[i] for r in rows]))
    rows.insert(0, header)
    rows.append(sum_row)
    return rows


def overweighted(bw, m, order_magnitude=10):
    if bw / (m or 1) > order_magnitude:
        return 1
    return 0


def underweighted(bw, m, order_magnitude=10):
    if m / (bw or 1) > order_magnitude:
        return 1
    return 0


def relays_in_bwfile_not_in_bwfile_other(file_created, other_file_created):
    bwfile = BwFile.objects.get(file_created=file_created)
    bwfile_other = BwFile.objects.get(file_created=other_file_created)
    relays_bw = bwfile.relays_here_not_in(bwfile_other)
    rows = []
    for rbw in relays_bw:
        try:
            other_rbw = RelayBw.objects.get(
                bwfile=bwfile_other, fingerprint=rbw.fingerprint
            )
        except RelayBw.DoesNotExist:
            other_rbw = None
        if other_rbw:
            if not rbw.error_circ:
                rbw.error_circ = other_rbw.error_circ
            if not rbw.error_stream:
                rbw.error_stream = other_rbw.error_stream

        row = [
            rbw.fingerprint,
            rbw.nickname,
            rbw.measured_at,
            rbw.error_circ,
            rbw.error_stream,
            rbw.relay.is_exit(),
        ]
        rows.append(row)
    sortedlist = sorted(
        rows, key=lambda row: (row[5] or False, row[4] or 0, row[3] or 0)
    )
    return sortedlist


def relays_in_bwfile_not_in_bwfile_other_header():
    return [
        "fp",
        "nickname",
        "measured at",
        "circuit errors",
        "stream errors",
        "is exit",
    ]
