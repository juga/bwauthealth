from django.urls import re_path

from . import views

urlpatterns = [
    re_path(
        r"^graph/cdf/(?:/(?P<bwauth_nickname>[\w-]+)/)?"
        "(?P<start>\d{4}-\d{2}-\d{2}T\d{2}:\d{2})/"  # noqa: W605
        "(?P<end>\d{4}-\d{2}-\d{2}T\d{2}:\d{2})/$",  # noqa: W605
        views.CdfGraph.as_view(),
        name="graph",
    ),
]
