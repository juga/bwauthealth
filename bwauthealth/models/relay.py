import logging

from onbasca.base.models.relay import RelayBase

logger = logging.getLogger(__name__)


class Relay(RelayBase):
    def nicknames(self):
        return self.relaydesc_set.all()

    def last_routerstatus(self):
        return self.routerstatus_set.order_by("published").last()
