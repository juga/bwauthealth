import datetime
import logging

from django.db import models, utils
from stem import Flag

from bwauthealth import util

from .relaybw import RelayBw
from .relaydesc import RelayDesc
from .relaytype import RelayType

logger = logging.getLogger(__name__)


class RelayBalanceRatioManager(models.Manager):
    def from_relay_descs(self, document):
        # logger.info("Importing RelayDescs %s", len(document))
        for rd in document:
            # logger.debug("%s", rd)
            self.from_relay_desc(rd)
            # logger.info("Imported RelayBalanceRatio.")

    def from_relay_desc(self, relay_desc):
        kwargs = {
            # "fingerprint": relay_desc.fingerprint,
            "published": relay_desc.published,
            "nickname": relay_desc.nickname,
            "observed_bandwidth": relay_desc.observed_bandwidth,
            "average_bandwidth": relay_desc.average_bandwidth,
            "burst_bandwidth": relay_desc.burst_bandwidth,
        }
        kwargs = dict(filter(lambda kv: kv[1] is not None, kwargs.items()))
        # When the routerstatuses are imported before, there're several with
        # the same fingerprint and valid_after, what changes is the bwauth
        rbrs = self.filter(
            fingerprint=relay_desc.fingerprint,
            valid_after__gt=relay_desc.published,
            published__lt=relay_desc.published,
        )
        rbrs.update(**kwargs)
        [rbr.set_min_descs_bw() for rbr in rbrs]

    def from_router_statuses(self, router_statuses):
        router_status = router_statuses[0]
        network_status = router_status.document
        # NOTE: Create votes only for bwauths, not dirauths.
        if not network_status.bandwidth_file_headers.get("timestamp", None):
            logger.debug("Not a bwauth")
            return
        kwargs = {
            "bwauth_nickname": network_status.directory_authorities[
                0
            ].nickname,
            "valid_after": network_status.valid_after.replace(
                minute=0, second=0
            ),
        }
        logger.info("Importing %s.", kwargs)
        if (
            self.filter(
                bwauth_nickname=kwargs["bwauth_nickname"],
                valid_after=kwargs["valid_after"],
            ).count()
            > 5000
        ):
            logger.debug("Probably imported already.")
            return
        for rs in router_statuses:
            rs = self.from_router_status(rs, **kwargs)
        # logger.info(
        #     "%s RelayBalanceRatio created/updated.", len(router_statuses)
        # )

    def from_router_status(self, router_status, **kwargs):
        kwargs.update(
            {
                "measured": (router_status.measured or router_status.bandwidth)
                * 1000,
                "nickname": router_status.nickname,
            }
        )
        valid_after = kwargs.pop("valid_after")
        bwauth_nickname = kwargs.pop("bwauth_nickname")
        # logger.info("Importing router status %s", router_status.fingerprint)
        try:
            rs, created = self.update_or_create(
                fingerprint=router_status.fingerprint,
                valid_after=valid_after,
                bwauth_nickname=bwauth_nickname,
                defaults=kwargs,
            )
        except utils.DataError as e:
            logger.warning("error: %s, kwargs %s", e, kwargs)
            return None
        else:
            # logger.info("RelayBalanceRatio %s, created: %s", rs, created)
            rs.set_relay_type(router_status.flags)
            return rs

    # When it's a bwfile and not a vote
    def from_bandwidth_file(
        self, bandwidth_file, bwauth_nickname=None, relay_fp=None
    ):
        if not bandwidth_file.header.get("file_created", None):
            logger.info("Not sbws.")
            return
        file_created = bandwidth_file.header["file_created"]
        valid_after = datetime.datetime.fromisoformat(file_created).replace(
            minute=0, second=0
        )
        bwauth_nick = util.select_bwauth(
            scanner_country=bandwidth_file.header["scanner_country"],
            file_created=datetime.datetime.fromisoformat(file_created)
            # destinations_countries=bandwidth_file.header['destinations_countries']
        )
        if not bwauth_nick:
            logger.info("Not sbws.")
            return
        logger.debug("bwauth %s", bwauth_nick)
        if bwauth_nickname and bwauth_nickname != bwauth_nick:
            return
        logger.info(
            "Importing %s bandwidth lines", len(bandwidth_file.measurements)
        )
        bwls = bandwidth_file.measurements.items()
        if relay_fp:
            bwls = [(relay_fp, bandwidth_file.measurements[relay_fp])]
        for fp, bwl in bwls:
            # logger.debug("Importing %s", bwl)
            if not int(bwl.get("vote", 1)):
                continue
            try:
                kwargs = {
                    "measured": int(bwl["bw"]) * 1000,
                    "nickname": bwl["nick"],
                    "observed_bandwidth": int(bwl.get("desc_bw_obs_last", 0)),
                    "average_bandwidth": int(bwl["desc_bw_avg"]),
                    "burst_bandwidth": int(bwl["desc_bw_bur"]),
                }
            except KeyError as e:
                logger.warning("%s, %s", e, bwl)
                continue
            try:
                rbr, created = RelayBalanceRatio.objects.update_or_create(
                    fingerprint=fp,
                    valid_after=valid_after,
                    bwauth_nickname=bwauth_nick,
                    defaults=kwargs,
                )
            except utils.DataError as e:
                logger.warning("%s, %s", e, kwargs)
                continue
            rbr.set_min_descs_bw()
            rbr.set_relay_balance_ratio()
            rbr.write_ratio()

    def from_relaybws_with_ratio(self):
        for rb in RelayBw.objects.filter(_ratio__isnull=False):
            RelayBalanceRatio.objects.update_or_create(
                fingerprint=rb.fingerprint,
                valid_after=rb.bwfile.file_created.replace(minute=0, second=0),
                bwauth_nickname=rb.bwfile.bwauth.nickname,
                defaults={"_ratio": rb._ratio},
            )

    def from_relaybws(self, rbws):
        for rbw in rbws:
            # logger.debug("Creating/updating %s", rbw)
            rbr, created = RelayBalanceRatio.objects.update_or_create(
                fingerprint=rbw.fingerprint,
                valid_after=rbw.bwfile.file_created.replace(
                    minute=0, second=0
                ),
                bwauth_nickname=rbw.bwfile.bwauth.nickname,
                defaults={
                    "nickname": rbw.nickname,
                    "measured": rbw.bw * 1000,
                    "observed_bandwidth": rbw.desc_bw_obs_last,
                    "average_bandwidth": rbw.desc_bw_avg,
                    "burst_bandwidth": rbw.desc_bw_bur,
                },
            )
            # logger.debug("rbr %s, created %s", rbr, created)
            rbr.set_min_descs_bw()


class RelayBalanceRatio(models.Model):
    """
    - vote: flags, weight, bwauth
    - descriptor: desc_bw_*, published
    """

    class Meta:
        get_latest_by = "published"
        unique_together = ("valid_after", "bwauth_nickname", "fingerprint")

    objects = RelayBalanceRatioManager()
    fingerprint = models.CharField(max_length=40)
    published = models.DateTimeField(editable=True, null=True, blank=True)
    nickname = models.CharField(max_length=255, null=True, blank=True)
    observed_bandwidth = models.PositiveIntegerField(null=True, blank=True)
    average_bandwidth = models.PositiveIntegerField(null=True, blank=True)
    burst_bandwidth = models.PositiveIntegerField(null=True, blank=True)
    _min_descs_bw = models.PositiveIntegerField(null=True, blank=True)

    measured = models.PositiveBigIntegerField(null=True, blank=True)
    valid_after = models.DateTimeField(null=True, blank=True)
    bwauth_nickname = models.CharField(max_length=19, null=True, blank=True)

    _relay_type = models.CharField(
        max_length=2,
        choices=RelayType.choices,
        default=RelayType.GUARD,
    )
    _ratio = models.FloatField(null=True, blank=True)

    def __str__(self) -> str:
        return "{}, {}".format(self.fingerprint, self.nickname)

    def ratio_str(self) -> str:
        return "{0.valid_after}, {0.bwauth_nickname}, {0.fingerprint}, "
        "{0._ratio}".format(self)

    def write_ratio(self):
        row = self.ratio_str() + "\n"
        with open("report/relay_ratios.csv", "a") as fd:
            fd.write(row)

    def set_relay_type(self, flags):
        if Flag.GUARD in flags:
            if Flag.EXIT in flags:
                self._relay_type = RelayType.GUARD_EXIT
            else:
                self._relay_type = RelayType.GUARD
        else:
            if Flag.EXIT in flags:
                self._relay_type = RelayType.EXIT
            else:
                self._relay_type = RelayType.MIDDLE
        self.save()
        return self._min_descs_bw

    def set_min_descs_bw(self):
        self._min_descs_bw = min(
            self.observed_bandwidth or 0,
            self.average_bandwidth or 0,
            self.burst_bandwidth or 0,
        )
        self.save()
        return self._min_descs_bw

    def set_relay_balance_ratio(self):
        # min_descs_bw = self.min_descs_bw()
        if not self._min_descs_bw:
            # logger.warning(
            #     "RelayBalanceRatio without descriptors bandwidth: %s", self
            # )
            # self.set_min_descs_bw()
            if not self._min_descs_bw:
                rbw = RelayBw.objects.filter(
                    fingerprint=self.fingerprint,
                    bwfile__file_created__lte=self.valid_after,
                    bwfile__bwauth__nickname=self.bwauth_nickname,
                )
                if rbw.count() >= 1:
                    rbw = rbw.latest()
                    if rbw.desc_bw_obs_last:
                        self.observed_bandwidth = rbw.desc_bw_obs_last
                        self.average_bandwidth = rbw.desc_bw_avg
                        self.burst_bandwidth = rbw.desc_bw_bur
                        self.save()
                        self.set_min_descs_bw()
                else:
                    rdesc = RelayDesc.objects.filter(
                        fingerprint=self.fingerprint,
                        published__lte=self.valid_after,
                    )
                    if rdesc:
                        if rdesc.count() > 1:
                            rdesc = rdesc.latest()
                        self.observed_bandwidth = rdesc.observed_bandwidth
                        self.average_bandwidth = rdesc.average_bandwidth
                        self.burst_bandwidth = rdesc.burst_bandwidth
                        self.save()
                        self.set_min_descs_bw()
            if not self._min_descs_bw:
                logger.warning("Couldn't set min bw for rbr %s", self.__dict__)
        if not self.measured:
            logger.warning("RelayBalanceRation without measured bw %s", self)
            return None
        # Multiply by 1000 since the scaled bw is divided by 1000
        ratio = self.measured / (self._min_descs_bw or 1)
        self._ratio = ratio
        self.save()
        logger.debug("ratio %s set for relay %s", self._ratio, self.nickname)
        return ratio
