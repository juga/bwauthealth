from django.db import models
from onbasca.base.models.base import BaseModel
from stem import Flag

from .relaytype import RelayType


class RelayRatio(BaseModel):

    relay_type = models.CharField(
        max_length=2,
        choices=RelayType.choices,
        default=RelayType.GUARD,
    )
    ratio = models.FloatField(null=True, blank=True)
    relaybw = models.OneToOneField(
        "RelayBw",
        on_delete=models.CASCADE,
        primary_key=True,
    )

    def set_relay_type(self, flags):
        if Flag.GUARD in flags:
            if Flag.EXIT in flags:
                self.relay_type = RelayType.GUARD_EXIT
            else:
                self.relay_type = RelayType.GUARD
        else:
            if Flag.EXIT in flags:
                self.relay_type = RelayType.EXIT
            else:
                self.relay_type = RelayType.MIDDLE
        self.save()
