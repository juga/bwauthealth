import datetime
import logging

from django.db import models
from onbasca.base.models import BaseModel

from . import BwAuth, BwFile

logger = logging.getLogger(__name__)


class VoteManager(models.Manager):
    def from_router_statuses(self, router_statuses, valid_after=None):
        router_status = router_statuses[0]
        if not valid_after:
            try:
                valid_after = router_status.document.valid_after
            except Exception:
                pass
        network_status = router_status.document
        dirauth = network_status.directory_authorities[0]
        bwauth = BwAuth.objects.from_directory_authority(dirauth)
        if not bwauth:
            return None
        bwfile = None
        # NOTE: Create votes only for bwauths, not dirauths.
        if not network_status.bandwidth_file_headers.get("timestamp", None):
            return None
        bwfile, created = BwFile.objects.update_or_create(
            file_created=datetime.datetime.utcfromtimestamp(
                int(network_status.bandwidth_file_headers["timestamp"])
            ),
            defaults={"bwauth": bwauth},
        )
        # NOTE: Create votes only for bwfiles already in the db.
        # if created:
        #     return None
        logger.info("BwFile %s created: %s", bwfile, created)
        vote, created = self.update_or_create(
            valid_after=network_status.valid_after,
            bwauth=bwauth,
            defaults={"bwfile": bwfile},
        )
        logger.info("Vote %s created: %s", vote, created)
        # NOTE: Not creating the router statuses, since votes are only imported
        # to know the bwauth of the bwfiles.
        # for rs in router_statuses:
        #     RouterStatus.objects.from_router_status(rs)
        # logger.info("%s Router statuses created", len(router_statuses))

        return vote


class Vote(BaseModel):
    class Meta:
        unique_together = ("valid_after", "bwauth")

    objects = VoteManager()
    valid_after = models.DateTimeField()
    bwauth = models.ForeignKey(
        "BwAuth", null=True, blank=True, on_delete=models.SET_NULL
    )
    bwfile = models.ForeignKey(
        "BwFile", null=True, blank=True, on_delete=models.SET_NULL
    )
    # When parsing the file from an existing/published one
    _filename = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "{}_{}".format(self.valid_after, self.bwauth.nickname)
