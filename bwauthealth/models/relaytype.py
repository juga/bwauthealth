from django.db import models


class RelayType(models.TextChoices):
    GUARD = "G", "Guard"
    MIDDLE = "M", "Middle"
    EXIT = "E", "Exit"
    GUARD_EXIT = "GE", "GuardExit"
