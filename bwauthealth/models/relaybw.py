import datetime
import logging

from django.db import models
from onbasca.base.models.relaybw import RelayBwBase, RelayBwManagerBase

from .relay import Relay
from .relaytype import RelayType

logger = logging.getLogger(__name__)


BWLINE_KEYS = [
    "node_id",
    "nick",
    "bw",
    "bw_mean",
    "bw_median",
    "consensus_bandwidth",
    "consensus_bandwidth_is_unmeasured",
    "desc_bw_avg",
    "desc_bw_bur",
    "desc_bw_obs_last",
    "desc_bw_obs_mean",
    "error_circ",
    "error_stream",
    "relay_in_recent_consensus_count",
    "relay_recent_measurement_attempt_count",
    "relay_recent_measurements_excluded_error_count",
    "relay_recent_priority_list_count",
    "success",
    "time",
    "measured_at",
    "updated_at",
    "vote",
    "under_min_report",
    "circ_fail",
    "pid_delta",
    "pid_bw",
    "pid_error",
    "pid_error_sum",
]
BWLINE2RELABW = {
    "node_id": "fingerprint",
    "nick": "nickname",
    # "time": "measured_at",
    # "measured_at": "updated_at"
}


class RelayBwManager(RelayBwManagerBase):
    def from_bwfile_bwline(self, bwline, bwfile=None):
        # for the files parsed with sbws, not stem
        if isinstance(bwline, dict):
            items = bwline.items()
        else:
            items = bwline.__dict__.items()
        # Keep only some keys for now
        kwargs = dict([(k, v) for k, v in items if k in BWLINE_KEYS])
        # Convert BwLine keys names to this object:
        for k, v in BWLINE2RELABW.items():
            kwargs[v] = kwargs.pop(k, None)
        if not kwargs.get("measured_at"):
            kwargs["measured_at"] = kwargs.pop("time", None)
        # In bwfile from sbws on 2021-09-04 19:35:40, there's a line that has
        # only bw and node_id.
        if kwargs["measured_at"] and len(kwargs["measured_at"]) == 10:
            kwargs["measured_at"] = datetime.datetime.utcfromtimestamp(
                int(kwargs["measured_at"])
            )
        if kwargs.get("updated_at"):
            if len(kwargs["updated_at"]) == 10:
                kwargs["updated_at"] = datetime.datetime.utcfromtimestamp(
                    int(kwargs["updated_at"])
                )
        if kwargs.get("vote", None) is None:
            kwargs["vote"] = 1
        fingerprint = kwargs.pop("fingerprint").replace("$", "")
        kwargs["relay"], _ = Relay.objects.get_or_create(
            fingerprint=fingerprint
        )
        relaybw, _ = self.update_or_create(
            fingerprint=fingerprint, bwfile=bwfile, defaults=kwargs
        )
        return relaybw


class RelayBw(RelayBwBase):

    objects = RelayBwManager()

    bwfile = models.ForeignKey(
        "BwFile", on_delete=models.CASCADE, null=True, blank=True
    )
    relay = models.ForeignKey(
        Relay, on_delete=models.CASCADE, null=True, blank=True
    )

    bw_mean = models.PositiveIntegerField(null=True, blank=True)
    bw_median = models.PositiveIntegerField(null=True, blank=True)
    desc_bw_obs_mean = models.PositiveIntegerField(null=True, blank=True)

    # Torflow KeyValues
    circ_fail = models.DecimalField(
        max_digits=13, decimal_places=11, null=True, blank=True
    )
    # measured_at = models.DateField(null=True, blank=True)
    pid_delta = models.DecimalField(
        max_digits=13, decimal_places=11, null=True, blank=True
    )
    pid_error = models.DecimalField(
        max_digits=13, decimal_places=11, null=True, blank=True
    )
    pid_error_sum = models.DecimalField(
        max_digits=13, decimal_places=11, null=True, blank=True
    )
    pid_bw = models.IntegerField(null=True, blank=True)
    pid_error = models.DecimalField(
        max_digits=13, decimal_places=11, null=True, blank=True
    )
    _min_descs_bw = models.PositiveIntegerField(null=True, blank=True)
    _ratio = models.FloatField(null=True, blank=True)
    _relay_type = models.CharField(
        max_length=2,
        choices=RelayType.choices,
        default=RelayType.GUARD,
    )

    def measured_within_bwfile_consensus(self):
        if self.bwfile.consensus:
            return self.measured_at > self.bwfile.consensus.valid_after and (
                self.measured_at
                - self.bwfile.consensus.valid_after
                + datetime.timedelta(hours=1)
            )
        return False

    def relay_was_in_bwfile_consensus(self):
        if self.bwfile.consensus:
            return self.bwfile.consensus in self.relay.consensuses.all()

    def relay_is_exit(self):
        return self.relay.is_exit

    #     def bwauth(self):
    #         return self.bwfile.bwauth

    def is_in_consensus(self):
        if self.bwfile.consensus:
            if self.bwfile.consensus.routerstatuses().filter(
                fingerprint=self.fingerprint
            ):
                return True
            else:
                return False
        return None

    def is_relaydesc_updated(self):
        if self.last_relaydesc():
            return (
                self.desc_bw_obs_last
                == self.last_relaydesc().observed_bandwidth
                and self.desc_bw_avg == self.last_relaydesc().average_bandwidth
                and self.desc_bw_bur == self.last_relaydesc().burst_bandwidth
            )
        return False

    def is_routerstatus_updated(self):
        if not self.relay:
            return None
        last_rs = self.relay.routerstatus_set.order_by("published").last()
        if last_rs:
            return (
                self.consensus_bandwidth == last_rs.bandwidth
                and self.consensus_bandwidth_is_unmeasured
                == last_rs.is_unmeasured
            )
        return None

    def min_descs_bw(self):
        # sbws
        if self.desc_bw_obs_last is not None:
            if self.desc_bw_avg:
                if self.desc_bw_bur:
                    min_descs_bw = min(
                        self.desc_bw_avg,
                        self.desc_bw_obs_last,
                        self.desc_bw_bur,
                    )
                    return min_descs_bw
                self._min_descs_bw = min(
                    self.desc_bw_avg, self.desc_bw_obs_last
                )
                self.save()
                return self._min_descs_bw
            if self.desc_bw_bur:
                self._min_descs_bw = min(
                    self.desc_bw_obs_last, self.desc_bw_bur
                )
                self.save()
                return self._min_descs_bw
            self._min_descs_bw = self.desc_bw_obs_last
            self.save()
            return self._min_descs_bw
        # Torflow
        # When calculationg ratios, this might get descriptors in the future
        self._min_descs_bw = self.relay.relaydescs_min_bandwidth_latest() or 1
        self.save()
        return self._min_descs_bw

    def set_relay_balance_ratio(self):
        min_descs_bw = self.min_descs_bw()
        if min_descs_bw is None:
            logger.warning("RelayLine without descriptors bandwidth: %s", self)
            return None
        # Multiply by 1000 since the scaled bw is divided by 1000
        ratio = self.bw * 1000 / (min_descs_bw or 1)
        self._ratio = ratio
        self.save()
        return ratio

    def set_relay_type(self):
        rs_last = self.relay_routerstatus_latest()
        if rs_last:
            # Not setting guard cause it's the default
            if rs_last._middle:
                self._relay_type = RelayType.MIDDLE
            elif rs_last._guard_exit:
                self._relay_type = RelayType.GUARD_EXIT
            elif rs_last._exit:
                self._relay_type = RelayType.EXIT
        self.save()
        return self._relay_type
