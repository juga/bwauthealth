import logging

from django.db import models
from onbasca.base.models.routerstatus import (
    RouterStatusBase,
    RouterStatusManagerBase,
)
from stem import Flag

from .relay import Relay

logger = logging.getLogger(__name__)


class RouterStatusManager(RouterStatusManagerBase):
    def from_router_status(self, router_status, consensus=None):
        # This is already in obasca, mv to base
        kwargs = {
            "_exit": Flag.EXIT in router_status.flags
            and Flag.GUARD not in router_status.flags,
            "_guard": Flag.GUARD in router_status.flags
            and Flag.EXIT not in router_status.flags,
            "_guard_exit": Flag.GUARD in router_status.flags
            and Flag.EXIT in router_status.flags,
            "_middle": Flag.GUARD not in router_status.flags
            and Flag.EXIT not in router_status.flags,
        }
        relay, _ = Relay.objects.get_or_create(
            fingerprint=router_status.fingerprint
        )
        relay.consensuses.add(consensus)
        kwargs["relay"] = relay
        rs = super().from_router_status(router_status, consensus, **kwargs)
        return rs


class RouterStatus(RouterStatusBase):
    objects = RouterStatusManager()
    consensus = models.ForeignKey(
        "Consensus", on_delete=models.CASCADE, null=True, blank=True
    )
    relay = models.ForeignKey(
        "Relay", on_delete=models.CASCADE, null=True, blank=True
    )
    # Already in onbasca, mv to base
    _exit = models.BooleanField(null=True, blank=True)
    _guard = models.BooleanField(null=True, blank=True)
    _guard_exit = models.BooleanField(null=True, blank=True)
    _middle = models.BooleanField(null=True, blank=True)
