"""
WSGI config for bwauthealthpr project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
import sys

# For django installed with pip
sys.path.append("/usr/local/lib/python3.7/dist-packages")
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bwauthealthpr.settings")

application = get_wsgi_application()
