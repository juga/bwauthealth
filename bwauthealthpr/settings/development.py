from .base import *

DEBUG = True

INSTALLED_APPS.append("django_extensions")

# shell_plus
SHELL_PLUS = "ipython"
# print SQL queries in shell_plus
SHELL_PLUS_PRINT_SQL = True
